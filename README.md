# slurm [![Build Status](https://travis-ci.com/hephyvienna/ansible-role-slurm.svg?branch=master)](https://travis-ci.com/hephyvienna/ansible-role-slurm) [![Ansible Role](https://img.shields.io/ansible/role/41059.svg)](https://galaxy.ansible.com/hephyvienna/slurm)

Install and configure [SLURM](https://slurm.schedmd.com/) and
[auks](https://github.com/hautreux/auks)

## Requirements

*   EL6/7
*   EPEL

## Role Variables

    slurm_roles: []

Roles of a host:
*   login - submit node
*   head - main slurm node
*   compute - workler node
*   slurmdb - Optional: slurmdb for accounting and fairshare
*   auks - Optional: auks for token renewal

It is possible to add several roles to a single node. It makes only sense
for _head_, _slurmdb_ and _auks_. 

    slurm_enable_auks: false

Enable token renewal with auks

    slurm_munge_key:

Location of the munge secret key. Can be created using

    dd if=/dev/urandom bs=1 count=1024 >/etc/munge/munge.key

and should be encoded using ansible-vault.

    slurm_repo:

Repository with slurm rpms. Its default value is the copr repo _dliko/slurm_.

    slurm_auks_repo:

Repository with auks rpm. Its default value is the copr repo _dliko/auks_.



## Example Playbook

    slurm_role: <role>
    slurm_munge_key: <munge.key>
    slurm_head_hosts:
      - <headnode fqan>
    slurm_db_host: <slurmdb fqan>
    slurm_db_storage_loc:
      - accounting
    slurm_db_storage_user: slurmdbd
    slurm_db_storage_pass: <secret>
    slurm_nodes:
      - name: compute
        CPUs: 1
        State: UNKNOWN
    slurm_partitions:
      - name: default
        Nodes: compute
        Default: YES
        MaxTime: INFINITE
        State: UP

## License

MIT

## Author Information

Written by [Dietrich Liko](http://hephy.at/dliko) in April 2019

[Institute for High Energy Physics](http://www.hephy.at) of the
[Austrian Academy of Sciences](http://www.oeaw.ac.at)
